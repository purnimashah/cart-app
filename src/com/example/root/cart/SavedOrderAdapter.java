package com.example.root.cart;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class SavedOrderAdapter extends ArrayAdapter<SavedOrder> {
	
	private static final String TAG = "SavedOrderAdapter";
    private List<SavedOrder> orderList = new ArrayList<SavedOrder>();
    SavedOrderViewHolder viewHolder;
    HashMap<String,Double> qmap = new HashMap<String, Double>();
    Context context;
    //Intent bill;
    Bundle b;

    HashMap<String,Double> quantities = new HashMap<String, Double>();

    public SavedOrderAdapter(Context context, int resource) {
        super(context, resource);
    }

    public SavedOrderAdapter(Context context, int resource, List<SavedOrder> orderList, Bundle bundle) {
        super(context, resource, orderList);
        this.orderList = orderList;
        this.context = context;
        b = bundle;
    }

    static class SavedOrderViewHolder {
        TextView orderId;
        TextView groupId;
        TextView items;
        TextView timeStamp;
    }

    @Override
    public void add(SavedOrder object) {
        orderList.add(object);
        super.add(object);
    }

    @Override
    public int getCount() {
        return this.orderList.size();
    }

    @Override
    public SavedOrder getItem(int index) {
        return this.orderList.get(index);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        if (row == null) {
            LayoutInflater inflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(com.example.root.cart.R.layout.saved_order, parent, false);
            viewHolder = new SavedOrderViewHolder();
            viewHolder.orderId = (TextView) row.findViewById(com.example.root.cart.R.id.saved_order_orderId);
            viewHolder.groupId = (TextView) row.findViewById(com.example.root.cart.R.id.saved_order_groupId);
            viewHolder.timeStamp = (TextView) row.findViewById(com.example.root.cart.R.id.saved_order_timeStamp);
            viewHolder.items = (TextView) row.findViewById(com.example.root.cart.R.id.saved_order_items);
            row.setTag(viewHolder);
        } else {
            viewHolder = (SavedOrderViewHolder)row.getTag();
        }
        SavedOrder order = getItem(position);
        Log.d("timepass-IV",order.getOrder_id()+" "+(viewHolder.orderId==null));
        viewHolder.orderId.setText(order.getOrder_id()+"");
        viewHolder.groupId.setText(order.getGroup_id()+"");
        viewHolder.timeStamp.setText(order.getTimeStamp());
        viewHolder.items.setText(order.getItems());

        //bill = new Intent(context,ConfirmOrderActivity.class);
        //bill.putExtras(b);
        return row;
    }

    public Bitmap decodeToBitmap(byte[] decodedByte) {
        return BitmapFactory.decodeByteArray(decodedByte, 0, decodedByte.length);
    }
}
