package com.example.root.cart;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;


@SuppressLint("NewApi")
public class NewOrderActivity extends Activity implements OnClickListener {

	Button b1,b2;
	EditText quantity;
	TextView itemTotal;
	TextView itemUnitPrice;
	int grandTotal; 
	Bundle bundle;
	Map <String,String> itemsMap = new HashMap<String, String>();
	Handler handler ;
	ListView list; 
	OrderRowAdapter adapter;
	ArrayList<ItemRow> itemss; 
	ArrayList<OrderRow> items; 
	String memberNumber; 
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(com.example.root.cart.R.layout.activity_new_order);

		b1=(Button)findViewById(com.example.root.cart.R.id.button_proceed_confirm);
		b2=(Button) findViewById(com.example.root.cart.R.id.button_draft);
		new LoginVerification(getApplicationContext(), itemsMap).execute("null");
		//getActionBar().setDisplayHomeAsUpEnabled(true);

		b1.setOnClickListener(this);
		b2.setOnClickListener(this); 
		//Log.d("Products"," continue oncreate");
		bundle = new Bundle();

		list = (ListView) findViewById(com.example.root.cart.R.id.new_order_item_list);
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
		String imgSett = prefs.getString("groupId", "");
		Log.d("neworderactivity",imgSett);
		
		List<OrderRow> order_item_list = new ArrayList<OrderRow>();
		adapter = new OrderRowAdapter(NewOrderActivity.this, com.example.root.cart.R.layout.order_row, order_item_list,bundle);
		list.setAdapter(adapter);
		//Log.d("Products"," after setadapter ");
		handler = new Handler(){

			@Override
			public void handleMessage(Message msg) {
				// TODO Auto-generated method stub
				super.handleMessage(msg);
				//Log.d("Products","in handler");
				if(getIntent().getSerializableExtra("itemsMap") != null){
					//Log.d("Order","bundle present");
					itemsMap = (Map) getIntent().getSerializableExtra("itemsMap");
					bundle = getIntent().getExtras();
				}

				List<OrderRow> order_item_list = getItems();
				//Log.d("Products",order_item_list.toString());
				adapter = new OrderRowAdapter(NewOrderActivity.this, com.example.root.cart.R.layout.order_row, order_item_list,bundle);
				list.setAdapter(adapter);
				adapter.notifyDataSetChanged();
			}

		};

	}

	public List<OrderRow> getItems() {
		
		items = new ArrayList<OrderRow>();

		Map<String, String> sortList = new TreeMap<String, String>(itemsMap);
		for(Entry<String, String> entry : sortList.entrySet()){
			OrderRow item = new OrderRow(entry.getKey(),Double.parseDouble(entry.getValue()),0.0);
			items.add(item);
		}
		//Log.d("Products","Items in items " + items.toString());
		return items;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(com.example.root.cart.R.menu.menu_dashboard, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();

		//noinspection SimplifiableIfStatement
		if (id == com.example.root.cart.R.id.action_home) {
			Intent i=new Intent(this,DashboardActivity.class);
			startActivity(i);
			return true;
		}

		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onClick(View view) {

		if(view == b1){
			   
				Intent i = new Intent(this,ConfirmOrderActivity.class);
				bundle.putSerializable("itemsMap",(Serializable)itemsMap);
				i.putExtras(bundle);
				startActivity(i);
			    }
			
		        

		if(view == b2){ 

			
			draft(bundle,itemsMap); 
			Intent i = new Intent(this,OrderDraftActivity.class); 
			bundle.putSerializable("itemsMap",(Serializable)itemsMap); 
			i.putExtras(bundle); 
			startActivity(i); 

		} 
	}
	
	public void draft(Bundle b, Map<String,String> ItemsMap) { 
    	 OrdersDraftDBHelper ordersDb = new OrdersDraftDBHelper(getApplicationContext()); 
        itemss = new ArrayList<ItemRow>(); 
      //  Map<String,String> itemsMap = (Map) getIntent().getSerializableExtra("itemsMap"); 
        Map<String, String> sortList = new TreeMap<String, String>(itemsMap); 
        Log.d("string value", sortList.size()+"");
       
        for(Map.Entry<String, String> entry : sortList.entrySet()){ 
            double quantity = b.getDouble(entry.getKey(),0); 
            if(quantity > 0) { 
                //save item to draft 
            	  double total = quantity * Double.parseDouble(entry.getValue());
                  grandTotal += total; 
                   ItemRow item = new ItemRow(entry.getKey(), quantity, Double.parseDouble(entry.getValue()), total); 
                  itemss.add(item); 
                  
            //	Log.d("Products","Items in items " + item); 
            } 
        } 
        try { 
 
            for (ItemRow item : itemss) { 
            	 SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext()); 
            	  memberNumber=sharedPref.getString("memberNumber",""); 
          
           	      ordersDb.insertOrder(Double.parseDouble(item.getUnitPrice().toString()),Double.parseDouble(item.getQuantity().toString()) ,Double.parseDouble(item.getTotal().toString()) ,item.getName().toString(),memberNumber);
                 
            } 
 
        } catch (Exception e) { 
            e.printStackTrace(); 
        } 
         
    } 
 
	 
 		

	@SuppressLint("NewApi")
	public class LoginVerification extends AsyncTask<String, String, Boolean> {

		//Map<String,String> itemsMap;

		public LoginVerification(Context context, Map<String, String> map){
			itemsMap = map;
		}

		@Override
		protected Boolean doInBackground(String... params) {

			String url = "http://ruralivrs.cse.iitb.ac.in/AFC/IvrsServlet?req_type=getProducts";
			JsonParser jParser = new JsonParser();
			JSONObject checkNumber = jParser.getJSONFromUrl(url);
			Boolean isPublisher = null;


			try {
				itemsMap = jParser.toMap(checkNumber.getJSONObject("products"));

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			//Log.d("Products", "" + itemsMap.toString());
			Message msg = Message.obtain();
			handler.sendMessage(msg);
			return isPublisher;
		}



	}
}
