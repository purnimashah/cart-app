package com.example.root.cart;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

public class OrderDraftActivity extends Activity implements View.OnClickListener{

    Button b1,b2;
    int grandTotal;
    ArrayList<ItemRow> items;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_draft_order);

        ListView list = (ListView) findViewById(R.id.confirm_order_item_list);
        List<ItemRow> item_list = getItems();
        ItemRowAdapter adapter = new ItemRowAdapter(this,R.layout.view_order_row, item_list);
        list.setAdapter(adapter);
        TextView totalBill = (TextView) findViewById(R.id.confirm_order_total_bill);
        totalBill.setText(grandTotal+"");

        b1=(Button)findViewById(R.id.button_modify_order);
        b2=(Button) findViewById(R.id.button1);
        b1.setOnClickListener(this);
        b2.setOnClickListener(this);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_dashboard, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_home) {
            Intent i = new Intent(this,DashboardActivity.class);
            startActivity(i);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public List<ItemRow> getItems() {

        items = new ArrayList<ItemRow>();

        

        Map<String,String> itemsMap = (Map) getIntent().getSerializableExtra("itemsMap");
        Map<String, String> sortList = new TreeMap<String, String>(itemsMap);
        Log.d("Products","Item Map in getItems is "+ itemsMap.toString());
        for(Map.Entry<String, String> entry : sortList.entrySet()){
            double quantity = getIntent().getDoubleExtra(entry.getKey(),0);
            if(quantity > 0) {
                double total = quantity *Double.parseDouble(entry.getValue());
                grandTotal += total;
                ItemRow item = new ItemRow(entry.getKey(), quantity, Double.parseDouble(entry.getValue()), total);
                items.add(item);
            }
        }
        Log.d("Products","Items in items " + items.toString());


        return items;
    }

    @Override
    public void onClick(View view) {
        if(view==b1){
            Intent i = new Intent(this,NewOrderActivity.class);
            i.putExtras(getIntent().getExtras());
            startActivity(i);
        }
        if(view==b2){
            Intent i = new Intent(this,DashboardActivity.class);
          //  i.putExtras(getIntent().getExtras());
            startActivity(i);
        }
        
      
    }
}

