package com.example.root.cart;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by root on 17/12/14.
 */
public class ItemRowAdapter extends ArrayAdapter<ItemRow> {

    private static final String TAG = "ItemRowArrayAdapter";
    private List<ItemRow> itemList = new ArrayList<ItemRow>();
    ItemRowViewHolder viewHolder;

    public ItemRowAdapter(Context context, int resource) {
        super(context, resource);
    }

    public ItemRowAdapter(Context context, int resource, List<ItemRow> itemList) {
        super(context, resource, itemList);
        this.itemList = itemList;
    }

    static class ItemRowViewHolder {
        TextView name;
        TextView quantity;
        TextView unitPrice;
        TextView total;
    }

    @Override
    public void add(ItemRow object) {
        itemList.add(object);
        super.add(object);
    }

    @Override
    public int getCount() {
        return this.itemList.size();
    }

    @Override
    public ItemRow getItem(int index) {
        return this.itemList.get(index);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;

        if (row == null) {
            LayoutInflater inflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(com.example.root.cart.R.layout.view_order_row, parent, false);
            viewHolder = new ItemRowViewHolder();
            viewHolder.name = (TextView) row.findViewById(com.example.root.cart.R.id.view_order_item_name);
            viewHolder.quantity = (TextView) row.findViewById(com.example.root.cart.R.id.view_order_item_quantity);
            viewHolder.unitPrice = (TextView) row.findViewById(com.example.root.cart.R.id.view_order_item_unit_rate);
            viewHolder.total = (TextView) row.findViewById(com.example.root.cart.R.id.view_order_item_bill);
            row.setTag(viewHolder);
        } else {
            viewHolder = (ItemRowViewHolder)row.getTag();
        }
        ItemRow item = getItem(position);
        viewHolder.name.setText(item.getName());
        viewHolder.quantity.setText(item.getQuantity().toString());
        viewHolder.unitPrice.setText(item.getUnitPrice().toString());
        viewHolder.total.setText(item.getTotal().toString() + "INR");

        return row;
    }

    public Bitmap decodeToBitmap(byte[] decodedByte) {
        return BitmapFactory.decodeByteArray(decodedByte, 0, decodedByte.length);
    }
}
