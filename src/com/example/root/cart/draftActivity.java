package com.example.root.cart;

import java.util.ArrayList;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class draftActivity extends Activity implements OnClickListener{

	public static String position;
	Button b1 , b2;
	int count=0;
	ListView list;
	ArrayAdapter<String> adapter;
	ArrayList<ItemRow> items;
	
	
	public void onCreate(Bundle savedInstanceState) {
		Log.d("purnima", "Inside onCreate");
		super.onCreate(savedInstanceState);
		SQLiteDatabase db = null;
		setContentView(R.layout.darft);
		
		list = (ListView)findViewById(R.id.listView1);
		b1 = (Button) findViewById(R.id.button);
		b2=(Button) findViewById(R.id.button_delete);
		List<String> msgList = getOrderDraft();
		adapter = new ArrayAdapter<String>(this,
        android.R.layout.simple_list_item_multiple_choice, msgList);
        list.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
        list.setAdapter(adapter);
        b1.setOnClickListener(this);
        b2.setOnClickListener(this);
	
	}


	public List<String> getOrderDraft() {
		List<String> orderDraft = new ArrayList<String>();
		//SQLiteDatabase db = null;
		SQLiteDatabase db = this.getReadableDatabase();
		OrdersDraftDBHelper dd = new OrdersDraftDBHelper(getBaseContext());
		
		dd.getWritableDatabase(); 
		db= openOrCreateDatabase("Order_drafts.db", SQLiteDatabase.CREATE_IF_NECESSARY, null); 
		//db= openOrCreateDatabase("FeedReader.db", SQLiteDatabase.CREATE_IF_NECESSARY, null);
		Log.d("purnima","test");
		try{
		Cursor cur = db.rawQuery("SELECT * FROM draftss", null);
		Log.d("purnima","test");
		if(cur!=null){
		while (cur.moveToNext()) {
			//String Name = cur.getString(cur.getColumnIndex("order_id"));
			String Name = cur.getString(cur.getColumnIndex("product_name"));
			String Price = cur.getString(cur.getColumnIndex("price"));
			String Quantity = cur.getString(cur.getColumnIndex("quantity"));
			String Total = cur.getString(cur.getColumnIndex("total"));  
			orderDraft.add("Name:" + Name + "\nQuantity:" + Quantity+ "  Price:" +Price+ "  Total:" +Total);
			//orderDraft.add(Name);
			
		}
		}
		else{
			Intent i = new Intent(this,NewOrderActivity.class);
            startActivity(i);
		}
		}
		catch(Exception e){
			Toast.makeText(draftActivity.this,"No Draft Order", Toast.LENGTH_LONG).show();
			 Log.d("TAG", " doesn't exist :(((");
		
		}
		return orderDraft;

	}

		private SQLiteDatabase getReadableDatabase() {
		// TODO Auto-generated method stub
		return null;
	}


		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			 SparseBooleanArray checked = list.getCheckedItemPositions();
		        ArrayList<String> selectedItems = new ArrayList<String>();
		        for (int i = 0; i < checked.size(); i++) {
		            // Item position in adapter
		            int position = checked.keyAt(i);
		            // Add sport if it is checked i.e.) == TRUE!
		            if (checked.valueAt(i))
		                selectedItems.add(adapter.getItem(position));
		        }
		 
		        String[] outputStrArr = new String[selectedItems.size()];
		        items = new ArrayList<ItemRow>();
		 
		        for (int i = 0; i < selectedItems.size(); i++) {
		            outputStrArr[i] = selectedItems.get(i);
		            int aa =outputStrArr[i].indexOf("\n");
		     		String name = outputStrArr[i].substring(5,aa);
		     		int aa1 =outputStrArr[i].indexOf("Quantity:");
		     		int aa2 = outputStrArr[i].indexOf("Price");
		     		int aa3 = outputStrArr[i].indexOf("Total");
		     		String quantity= outputStrArr[i].substring(aa1+9 , aa2-2);
		     		String price = outputStrArr[i].substring(aa2+6, aa3-2);
		     		String total = outputStrArr[i].substring(aa3+6);
		     		
		     		Log.d("test string",name);
		     		Log.d("test string",total);
		     		ItemRow item = new ItemRow(name , Double.parseDouble(quantity), Double.parseDouble(price),Double.parseDouble(total));
		     		Log.d("purnima", "item add");
	                items.add(item);
		        }
		 
			if(v == b1){
			
				int length= outputStrArr.length;
				
		       Log.d("test+++", length+"");
		       
		       if(length == 0){
		    	   Toast.makeText(getApplicationContext(), "Please select atleast one product",
		    			   Toast.LENGTH_LONG).show();
		    	   
		       }
		       else {
				Bundle b = new Bundle();
		        b.putStringArray("selectedItems", outputStrArr);
				Intent intent = new Intent(getApplicationContext(),DraftToOrder.class);
		        intent.putExtras(b);
		 	    startActivity(intent);
		       }
			}
			if(v == b2){
	        	
				int j =1;
				SQLiteDatabase db = this.getReadableDatabase();
				OrdersDraftDBHelper dd = new OrdersDraftDBHelper(getBaseContext());
				dd.getWritableDatabase(); 
				
				db= openOrCreateDatabase("Order_drafts.db", SQLiteDatabase.CREATE_IF_NECESSARY, null);
				Log.d("itemrow",items.size()+"");
							
				 for(ItemRow item : items){
					 
					 Log.d("testing",item.getName());
					 
					
		            	Log.d("Delete Query = ", "Delete from draftss where COLUMN_NAME_PRODUCT_NAME="+item.getName() +" AND"+" quantity="+item.getQuantity());
		            db.execSQL("Delete from draftss where product_name ='"+item.getName()+"'AND quantity='"+item.getQuantity()+"'");
		                j++;
		                Toast.makeText(draftActivity.this,"Draft Order deleted succssfully", Toast.LENGTH_LONG).show();
						 Intent intent = new Intent(getApplicationContext(),
					                DashboardActivity.class);
						 startActivity(intent);
		              
				 }
				
				
				 }
				 
			}
				
			}
		


