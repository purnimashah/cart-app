package com.example.root.cart;

import android.app.Activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;


public class DashboardActivity extends Activity implements OnClickListener , OnItemSelectedListener{

    ImageButton newOrderButton, previousOrderButton,draftButton,settingButton,orderCancelButton;
    Handler handler;
    EditText phoneNumber;
    LoginVerification loginVerification;
    String memberNumber="";
    int[] groups={};
    SharedPreferences sharedPref;
    Spinner group_spinner , subgroup_spinner;
    ArrayAdapter<String>adapter_state;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    
        sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        SharedPreferences.Editor editor = sharedPref.edit();
        
        boolean registered = sharedPref.getBoolean("registered", false);
        boolean isPublisher = Boolean.valueOf(sharedPref.getString("publisher", "false"));
        handler = new Handler(){
            @Override
            public void handleMessage(Message msg) {

                Log.d("Parag","Message is : "+ msg.obj.toString());
                
                if(msg.obj.toString().equalsIgnoreCase("false")){

                    Toast.makeText(getApplicationContext(), "You are not a registered Member.", Toast.LENGTH_LONG).show();

                }
                else{
                    SharedPreferences.Editor editor = sharedPref.edit();
                    editor.putBoolean("registered", true);
                    editor.putString("memberNumber", memberNumber);
                    //editor.putInt("group",groups[0]);
                    editor.commit();
                    Log.d("Parag", sharedPref.getAll().toString());
                    setContentView(com.example.root.cart.R.layout.activity_dashboard);
                    finish();
                    startActivity(getIntent());
                    
                }
            }
        };

        if(registered){
            setContentView(com.example.root.cart.R.layout.activity_dashboard);
            newOrderButton=(ImageButton)findViewById(com.example.root.cart.R.id.button_new_order);
            previousOrderButton=(ImageButton)findViewById(com.example.root.cart.R.id.button_prev_orders);
            
            draftButton=(ImageButton)findViewById(com.example.root.cart.R.id.button_help);
            settingButton=(ImageButton)findViewById(com.example.root.cart.R.id.button_settings);
            orderCancelButton=(ImageButton) findViewById(com.example.root.cart.R.id.button_order_cancel);
            group_spinner= (Spinner) findViewById(com.example.root.cart.R.id.group_spinner);
            subgroup_spinner=(Spinner) findViewById(com.example.root.cart.R.id.subgroup_spinner);
            group_spinner.setOnItemSelectedListener(this);

            newOrderButton.setOnClickListener(DashboardActivity.this);
            previousOrderButton.setOnClickListener(DashboardActivity.this);
            draftButton.setOnClickListener(DashboardActivity.this);
            settingButton.setOnClickListener(DashboardActivity.this);
            orderCancelButton.setOnClickListener(DashboardActivity.this);
        }
        
       
        else
        {
            setContentView(com.example.root.cart.R.layout.login);
            phoneNumber = (EditText)findViewById(com.example.root.cart.R.id.phoneNumber);

        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(com.example.root.cart.R.menu.menu_dashboard, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
       
        //noinspection SimplifiableIfStatement
        switch (item.getItemId()) {
            case com.example.root.cart.R.id.action_home:
                return true;
            case com.example.root.cart.R.id.action_settings:
            	Intent intent = new Intent(this,OverflowActivity.class);
            	startActivity(intent);
            //case R.id.action_back:
             //   return true;
            default:
                return super.onOptionsItemSelected(item);
        }

        //return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
    	// for internet connectivity
       ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
   	   NetworkInfo wifiNetwork = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
   	   NetworkInfo mobileNetwork = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
    	
   	   if(view== newOrderButton){
   		if(mobileNetwork != null && mobileNetwork.isConnected() || wifiNetwork != null && wifiNetwork.isConnected()){
        	
            Intent i = new Intent(this,NewOrderActivity.class);
            startActivity(i);
        	}
        	else{
        		
        		Toast.makeText(DashboardActivity.this,"No internet connection", Toast.LENGTH_LONG).show();
        		Intent i = new Intent(this,DashboardActivity.class);
                startActivity(i);
        		
        	}
        }
        else if(view ==  previousOrderButton){
        	
        	if(mobileNetwork != null && mobileNetwork.isConnected() || wifiNetwork != null && wifiNetwork.isConnected()){
            	
        		 Intent i = new Intent(this,PreviousOrdersActivity.class);
                 startActivity(i);
            	}
            	else{
            		
            		Toast.makeText(DashboardActivity.this,"No internet connection", Toast.LENGTH_LONG).show();
            		Intent i = new Intent(this,DashboardActivity.class);
                    startActivity(i);
            		
            	}
           
        }
        else if(view == draftButton){
            Intent i = new Intent(this,draftActivity.class);
            startActivity(i);
        }
        else if(view == settingButton){
            Intent i = new Intent(this,SettingsActivity.class);
            startActivity(i);
        }
 
        else if(view == orderCancelButton){
        	if(mobileNetwork != null && mobileNetwork.isConnected() || wifiNetwork != null && wifiNetwork.isConnected()){
        		Intent i = new Intent(this,CancelOrder.class);
                startActivity(i);
        	}
        	else{
        		Toast.makeText(DashboardActivity.this,"No internet connection", Toast.LENGTH_LONG).show();
        		Intent i = new Intent(this,DashboardActivity.class);
                startActivity(i);
        	}
        
        }

    }

    public class LoginVerification extends AsyncTask<String, String, Boolean> {

        ProgressDialog pd;
        Context context;
        Handler handler;

        public LoginVerification(Context context, Handler handler) {
            // TODO Auto-generated constructor stub
            this.context = context;
            this.handler = handler;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(context);
            pd.setMessage("Wait");
            pd.show();
        }

        @Override
        protected Boolean doInBackground(String... params) {

            String url1 = "http://ruralivrs.cse.iitb.ac.in/AFC/IvrsServlet?req_type=memberGroups&number=" + params[0];
            JsonParser jParser = new JsonParser();
            JSONObject checkNumber = jParser.getJSONFromUrl(url1);
            String url = "http://ruralivrs.cse.iitb.ac.in/AFC/IvrsServlet?req_type=getProducts";

            memberNumber = params[0];
            JSONObject productsFromDB = jParser.getJSONFromUrl(url);
            Boolean isPublisher = false;
            Map<Integer, String> productsMap = null;
            try {
                if(checkNumber.get("groups").toString().equals("[]")){
                    pd.dismiss();
                    Log.d("test",checkNumber.get("groups").toString());
                }
                else{
                    
                    isPublisher=true;
                    try {
                    	String groupsId=checkNumber.get("groups").toString();
                    	String groupId;
                    	int index = groupsId.indexOf("[");
                    	int index2= groupsId.indexOf(",");
                    	Log.d("test",index2+"");
                    	
                    	if(index2 <0){
                    		int index3= groupsId.indexOf("]");
                    		 groupId=groupsId.substring(index+2,index3-1);
                    		 
                    	}
                    	else {
                    	     groupId = groupsId.substring(index+2, index2-1);
                    	
                    	}
                    	
                    	 SharedPreferences.Editor editor = sharedPref.edit();
                         
                         editor.putString("groupId", groupId);
                         editor.commit();
                        productsMap = jParser.toMap(productsFromDB.getJSONObject("products"));

                    } catch (JSONException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }

            }catch (JSONException e1) {
                e1.printStackTrace();
            }
            return isPublisher;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            // TODO Auto-generated method stub

            Message msg = Message.obtain();
            msg.obj = result.toString();
            handler.sendMessage(msg);
            pd.dismiss();

        }
    }
    public void newOrder(View v){
        Intent i = new Intent(this,NewOrderActivity.class);
        startActivity(i);
    }

    public void previousOrder(View v){
        Intent i = new Intent(this,PreviousOrdersActivity.class);
        startActivity(i);
    }

    public void draft(View v){
        Intent i = new Intent(this,draftActivity.class);
        startActivity(i);
    }

    public void settings(View v){
        Intent i = new Intent(this,SettingsActivity.class);
        startActivity(i);
    }

    public void login(View v){

        String number = phoneNumber.getText().toString();
		      
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
 	   NetworkInfo wifiNetwork = connectivityManager
 	     .getNetworkInfo(ConnectivityManager.TYPE_WIFI);
 	   NetworkInfo mobileNetwork = connectivityManager
 			     .getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
     	    	
     	 if(number.equalsIgnoreCase("")){
            Toast.makeText(getApplicationContext(), "Enter Valid number", Toast.LENGTH_SHORT).show();

        }
        else {
            long i = Long.parseLong(number);
            long length = (long)(Math.log10(i)+1);
            if(length<10 || length > 10){
                Toast.makeText(getApplicationContext(), "Enter Valid number", Toast.LENGTH_SHORT).show();
            }
            else{
            	if(mobileNetwork != null && mobileNetwork.isConnected() || wifiNetwork != null && wifiNetwork.isConnected()){
                loginVerification = new LoginVerification(DashboardActivity.this, handler);
                loginVerification.execute(number);
            	}
            	else{
            		 Toast.makeText(getApplicationContext(), "Please check internet connection", Toast.LENGTH_SHORT).show();
            		
            	}
            }
        }
    }

	@Override
	public void onItemSelected(AdapterView<?> parent, View view, int position,
			long id) {
		Log.d("testing","inside selection");
		
		// TODO Auto-generated method stub
		group_spinner.setSelection(position);
		String item = (String) group_spinner.getSelectedItem();
		Log.d("naam ",item);
		String subgroup[] = {"test", "abc" , "cde"};
		String subgroup_afc[] = {"testing", "pune" ,"IIT"};
		String [] groups=new String[subgroup.length];
				
		if(item.equals("MOFF")){
			
			Log.d("test","inside Moff");
			adapter_state = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item, subgroup); 
			subgroup_spinner.setAdapter(adapter_state);
		}
		
      if(item.equals("AFC")){
			
			Log.d("test","inside AFC");
			adapter_state = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item, subgroup_afc); 
			subgroup_spinner.setAdapter(adapter_state);
		}
		//adapter_state.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		
		
	}

	@Override
	public void onNothingSelected(AdapterView<?> parent) {
		// TODO Auto-generated method stub
		
	}
	
	public String connect(){
		String connection= "false";
	//	ConnectivityManager connectivityManager = (ConnectivityManager) this.context.getSystemService(Context.CONNECTIVITY_SERVICE);
		 ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
	   	   NetworkInfo wifiNetwork = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
	   	   NetworkInfo mobileNetwork = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
	   	if(mobileNetwork != null && mobileNetwork.isConnected() || wifiNetwork != null && wifiNetwork.isConnected()){
	   		
	   		connection="true";
	   		return connection;
	   	}
	   	else {
	   		return connection;
	   	}
		
		
	}
}
