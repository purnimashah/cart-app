package com.example.root.cart;

 


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;

/**
 * Created by root on 21/1/15.
 */
public class OrdersDraftDBHelper extends SQLiteOpenHelper {
    // If you change the database schema, you must increment the database version.
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "Order_drafts.db";
    public static final String TABLE_NAME = "draftss";

    public final class OrdersEntry implements BaseColumns{
        
        public static final String TABLE_NAME = "draftss";
      //  public static final String COLUMN_NAME_PRODUCT_ID = "product_id";
        public static final String COLUMN_NAME_PRODUCT_NAME = "product_name";
        public static final String COLUMN_NAME_MEMBER_NUMBER = "member_num";
        public static final String COLUMN_NAME_PRICE = "price";
        public static final String COLUMN_NAME_TOTAL = "total";
        public static final String COLUMN_NAME_QUANTITY = "quantity";

    }

    
  /*  public static final String SQL_CREATE_ENTRIES =
    		
            "CREATE TABLE " + OrdersEntry.TABLE_NAME + " (" +
           
            OrdersEntry.COLUMN_NAME_PRODUCT_NAME + " TEXT NOT NULL , " +
            OrdersEntry.COLUMN_NAME_PRICE + " INTEGER NOT NULL," +
           OrdersEntry.COLUMN_NAME_TOTAL + " INTEGER NOT NULL," +
           OrdersEntry.COLUMN_NAME_QUANTITY + " INTEGER NOT NULL," +
            OrdersEntry.COLUMN_NAME_MEMBER_NUMBER + " TEXT NOT NULL " +
            ")";

    public static final String SQL_DELETE_ENTRIES =
            " DROP TABLE IF EXISTS " + OrdersEntry.TABLE_NAME;*/

    public OrdersDraftDBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
    public void onCreate(SQLiteDatabase db) {
        //db.execSQL(SQL_CREATE_ENTRIES);
    	db.execSQL("CREATE TABLE " + OrdersEntry.TABLE_NAME + " (" +
           
            OrdersEntry.COLUMN_NAME_PRODUCT_NAME + " TEXT NOT NULL , " +
            OrdersEntry.COLUMN_NAME_PRICE + " INTEGER NOT NULL," +
           OrdersEntry.COLUMN_NAME_TOTAL + " INTEGER NOT NULL," +
           OrdersEntry.COLUMN_NAME_QUANTITY + " INTEGER NOT NULL," +
            OrdersEntry.COLUMN_NAME_MEMBER_NUMBER + " TEXT NOT NULL " +
            ")");
    
    }
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // TODO: Tasks to perform On Upgrde
        //db.execSQL(SQL_DELETE_ENTRIES);
        db.execSQL(" DROP TABLE IF EXISTS " + OrdersEntry.TABLE_NAME);
    	onCreate(db);
    }
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }

  
    public long insertOrder(double unitPrice, double quantity ,double total, String name,String memberNumber) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(OrdersEntry.COLUMN_NAME_PRODUCT_NAME, name);
        values.put(OrdersEntry.COLUMN_NAME_PRICE, unitPrice);
        values.put(OrdersEntry.COLUMN_NAME_TOTAL, total);
        values.put(OrdersEntry.COLUMN_NAME_MEMBER_NUMBER, memberNumber);
        values.put(OrdersEntry.COLUMN_NAME_QUANTITY, quantity);

        long newRowID = db.insert(OrdersEntry.TABLE_NAME, null, values);

        return newRowID;
    }
}
