package com.example.root.cart;

 import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class OverflowActivity extends Activity {
    ListView listView ;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.overflow_list);
        
        // Get ListView object from xml
        listView = (ListView) findViewById(R.id.list);
        
        // Defined Array values to show in ListView
        String[] values = new String[] { "About Us", 
                                         "FAQ",
                                         "Contact Us",
                                         "More", 
                                        };

        // Define a new Adapter
        // First parameter - Context
        // Second parameter - Layout for the row
        // Third parameter - ID of the TextView to which the data is written
        // Forth - the Array of data

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
          android.R.layout.simple_list_item_1, android.R.id.text1, values);


        // Assign adapter to ListView
        listView.setAdapter(adapter); 
        
        // ListView Item Click Listener
        listView.setOnItemClickListener(new OnItemClickListener() {

              @Override
              public void onItemClick(AdapterView<?> parent, View view,
                 int position, long id) {
                
               // ListView Clicked item index
               int itemPosition     = position;
               
               // ListView Clicked item value
               String  itemValue    = (String) listView.getItemAtPosition(position);
                  
                // Show Alert 
                //Toast.makeText(getApplicationContext(),
                  //"Position :"+itemPosition+"  ListItem : " +itemValue + "Id"+id, Toast.LENGTH_LONG)
                  //.show();
                if(itemValue.equals("FAQ")){
                	Toast.makeText(getApplicationContext(),
                            "FAQ", Toast.LENGTH_LONG)
                            .show();
                } else if(itemValue.equals("Contact Us")){
                	Toast.makeText(getApplicationContext(),
                            "Contact Us", Toast.LENGTH_LONG)
                            .show();
                	Uri uriUrl = Uri.parse("http://ruralivrs.cse.iitb.ac.in/AFC/Document.jsp#no-back-button");
                	Intent launchBrowser = new Intent(Intent.ACTION_VIEW, uriUrl);
                	startActivity(launchBrowser);
                } else if(itemValue.equals("About Us")){
                	Toast.makeText(getApplicationContext(),
                            "About Us", Toast.LENGTH_LONG)
                            .show();
                	Intent intent = new Intent(OverflowActivity.this,AboutActivity.class);
                	startActivity(intent);
                } 
                else{ 
                	Toast.makeText(getApplicationContext(),
                            "More", Toast.LENGTH_LONG)
                            .show();
                }
             
              }

         }); 
    }

}
