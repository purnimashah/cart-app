package com.example.root.cart;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by root on 18/12/14.
 */
public class OrderRowAdapter extends ArrayAdapter<OrderRow>{

    private static final String TAG = "OrderRowArrayAdapter";
    private List<OrderRow> orderItemList = new ArrayList<OrderRow>();
    OrderRowViewHolder viewHolder;
    HashMap<String,Double> qmap = new HashMap<String, Double>();
    Context context;
    //Intent bill;
    Bundle b;

    HashMap<String,Double> quantities = new HashMap<String, Double>();

    public OrderRowAdapter(Context context, int resource) {
        super(context, resource);
    }

    public OrderRowAdapter(Context context, int resource, List<OrderRow> orderItemList, Bundle bundle) {
        super(context, resource, orderItemList);
        this.orderItemList = orderItemList;
        this.context = context;
        b = bundle;
    }

    static class OrderRowViewHolder {
        TextView name;
        TextView unitPrice;
        EditText quantity;
        TextView total;
    }

    @Override
    public void add(OrderRow object) {
        orderItemList.add(object);
        super.add(object);
    }

    @Override
    public int getCount() {
        return this.orderItemList.size();
    }

    @Override
    public OrderRow getItem(int index) {
        return this.orderItemList.get(index);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        if (row == null) {
            LayoutInflater inflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(com.example.root.cart.R.layout.order_row, parent, false);
            viewHolder = new OrderRowViewHolder();
            viewHolder.name = (TextView) row.findViewById(com.example.root.cart.R.id.new_order_item_name);
            viewHolder.unitPrice = (TextView) row.findViewById(com.example.root.cart.R.id.new_order_item_unit_rate);
            viewHolder.quantity = (EditText) row.findViewById(com.example.root.cart.R.id.new_order_item_quantity);
            viewHolder.total = (TextView) row.findViewById(com.example.root.cart.R.id.new_order_item_bill);
            row.setTag(viewHolder);
        } else {
            viewHolder = (OrderRowViewHolder)row.getTag();
        }
        OrderRow item = getItem(position);
        viewHolder.name.setText(item.getName());
        viewHolder.unitPrice.setText(item.getUnitPrice().toString());

        double quantity;
        if(qmap.containsKey(item.getName())){
            quantity = qmap.get(item.getName());
        }
        else if(b.containsKey(item.getName())){
            quantity = b.getDouble(item.getName(),0);
        }
        else{
            quantity = 0;
            qmap.put(item.getName(),0.0);
            Log.d("cart","Name is " + item.getName());
        }

        viewHolder.quantity.setText(quantity+"");

        item.setTotal(quantity*item.getUnitPrice());
        viewHolder.total.setText(item.getTotal().toString() + "INR");

        final EditText quantity_field = (EditText) row.findViewById(com.example.root.cart.R.id.new_order_item_quantity);
        final TextView total_field = (TextView) row.findViewById(com.example.root.cart.R.id.new_order_item_bill);
        final TextView rate_field = (TextView) row.findViewById(com.example.root.cart.R.id.new_order_item_unit_rate);
        final TextView item_name = (TextView) row.findViewById(com.example.root.cart.R.id.new_order_item_name);

        //bill = new Intent(parent.getContext(),ConfirmOrderActivity.class);
        //final Bundle b = new Bundle();
        //Log.d("cart","Intent" + bill.toString());

        quantity_field.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
                System.out.println("inside listener");
                if(s.length() != 0)
                {
                    Log.d("cart", "inside listener");
                    System.out.println("inside listener");
                    total_field.setText(Double.parseDouble(rate_field.getText().toString()) * Double.parseDouble(quantity_field.getText().toString()) + " INR");

                    //bill.putExtra(item_name.getText().toString(),Double.parseDouble(quantity_field.getText().toString()));
                    b.putDouble(item_name.getText().toString(),Double.parseDouble(quantity_field.getText().toString()));
                    qmap.put(item_name.getText().toString(),Double.parseDouble(quantity_field.getText().toString()));

                    //quantities.put(item_name.getText().toString(),Double.parseDouble(quantity_field.getText().toString()));


                    //QMap.getInstance().putValue(item_name.getText().toString(), Double.parseDouble(quantity_field.getText().toString()));

                    Log.d("cart", item_name.getText().toString() + " " + quantity_field.getText().toString());
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
        });

        //bill = new Intent(context,ConfirmOrderActivity.class);
        //bill.putExtras(b);
        return row;
    }

    public Bitmap decodeToBitmap(byte[] decodedByte) {
        return BitmapFactory.decodeByteArray(decodedByte, 0, decodedByte.length);
    }
}
