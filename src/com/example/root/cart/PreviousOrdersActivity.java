package com.example.root.cart;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.Map.Entry;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.example.root.cart.NewOrderActivity.LoginVerification;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;


public class PreviousOrdersActivity extends Activity implements OnClickListener {

	Bundle bundle;
	Map <String,String> itemsMap = new HashMap<String, String>();
	Handler handler ;
	ListView orderList; 
	SavedOrderAdapter adapter;
	ArrayList<JSONObject> orderObjects; 
	ArrayList<SavedOrder> orders; 
	String memberNumber; 

	//String num_orders = sharedPref.getString("num_orders", "");
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_previous_orders);

		new FetchOrders(getApplicationContext(), itemsMap).execute("null");
		//getActionBar().setDisplayHomeAsUpEnabled(true);

		//Log.d("Products"," continue oncreate");
		bundle = new Bundle();

		orderList = (ListView) findViewById(R.id.order_list);
		List<SavedOrder> saved_order_list = new ArrayList<SavedOrder>();
		adapter = new SavedOrderAdapter(PreviousOrdersActivity.this,R.layout.saved_order, saved_order_list,bundle);
		orderList.setAdapter(adapter);
		orderList.setOnItemClickListener(new OnItemClickListener() {
	        public void onItemClick(AdapterView<?> parent, View view,
	                int position, long id) {

	       Object o = orderList.getItemAtPosition(position);
	       Log.d("orderList",position+"");
	       Intent i = new Intent(getApplicationContext(),ViewOrderActivity.class);
	       SavedOrder order = orders.get(position);
	       Log.d("timePass V",order.toString());
	       i.putExtra("SavedOrderItems", (Serializable)order.getItemList());
	       i.putExtra("SavedOrderId",order.getOrder_id());
	       i.putExtra("SavedOrderGroupId",order.getGroup_id());
	       i.putExtra("SavedOrderTime",order.getTimeStamp());
	       startActivity(i);
	       }
	    });//Log.d("Products"," after setadapter ");
		handler = new Handler(){

			@Override
			public void handleMessage(Message msg) {
				// TODO Auto-generated method stub
				super.handleMessage(msg);
				//Log.d("Products","in handler");
				if(getIntent().getSerializableExtra("itemsMap") != null){
					//Log.d("Order","bundle present");
					itemsMap = (Map) getIntent().getSerializableExtra("itemsMap");
					bundle = getIntent().getExtras();
				}

				ArrayList<SavedOrder> saved_order_list = getOrders();
				//Log.d("Products",order_item_list.toString());
				adapter = new SavedOrderAdapter(PreviousOrdersActivity.this,R.layout.saved_order, saved_order_list,bundle);
				orderList.setAdapter(adapter);
				adapter.notifyDataSetChanged();
			}

		};

	}

	public ArrayList<SavedOrder> getOrders() {
		
		orders = new ArrayList<SavedOrder>();

		for(JSONObject entry : orderObjects){
			SavedOrder order = new SavedOrder(entry,memberNumber);
			orders.add(order);
		}
		//Log.d("Products","Items in items " + items.toString());
		return orders;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.menu_dashboard, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();

		//noinspection SimplifiableIfStatement
		if (id == R.id.action_home) {
			Intent i=new Intent(this,DashboardActivity.class);
			startActivity(i);
			return true;
		}

		return super.onOptionsItemSelected(item);
	}

	@SuppressLint("NewApi")
	public class FetchOrders extends AsyncTask<String, String, Boolean> {

		//Map<String,String> itemsMap;

		public FetchOrders(Context context, Map<String, String> map){
			itemsMap = map;
		}

		@Override
		protected Boolean doInBackground(String... params) {
			
			SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
            memberNumber=sharedPref.getString("memberNumber","");
            
          //  SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
            String imgSett = sharedPref.getString("groupId", "0");
           
            String limit = sharedPref.getString("num_orders_stored","0");
            String number = sharedPref.getString("orders_sort_by","0");
    	    Log.d("caer",imgSett);
    	    Log.d("number ",number);
    	    int num = number.indexOf(",");
    	    
    	    String orderby=number.substring(num+2);
    	    Log.d("numbererer",orderby);
    	    
    	    if (orderby.startsWith("a")){
    	    	number ="ASC";
    	    }
    	    else{
    	    	number="DESC";
    	    }
    	    
    	    Log.d("type", number);
            String url = "http://ruralivrs.cse.iitb.ac.in/AFC/IvrsServlet?req_type=getOrders&number="+memberNumber+"&limit="+limit+"&str="+number;
			Log.d("string url", url);
            JsonParser jParser = new JsonParser();
			JSONObject orderList = jParser.getJSONFromUrl(url);
		//	Log.d("timepass", orderList.toString());
			Boolean isPublisher = null;
			orderObjects=new ArrayList<JSONObject>();


			try {
				JSONArray ordersArray = orderList.getJSONArray("orders");
				for(int i=0;i<ordersArray.length();i++) {
					orderObjects.add((JSONObject)ordersArray.get(i));
					Log.d("timePass-II", ordersArray.get(i).toString());
				}

				Log.d("timePass-III", orderObjects.size()+"");
				
			//	itemsMap = jParser.toMap(checkNumber.getJSONObject("products"));

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			//Log.d("Products", "" + itemsMap.toString());
			Message msg = Message.obtain();
			handler.sendMessage(msg);
			return isPublisher;
		}



	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		
	}

}
