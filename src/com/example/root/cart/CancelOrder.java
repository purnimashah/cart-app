package com.example.root.cart;

import java.util.ArrayList;

import java.util.HashMap;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.SharedPreferences;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;

import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.util.SparseArray;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnGroupClickListener;
import android.widget.ExpandableListView.OnGroupExpandListener;
import android.widget.ListView;
import android.widget.Toast;
public class CancelOrder  extends Activity {
	
	//code_other
	SharedPreferences sharedPref;
	private ProgressDialog pDialog;
	ListView lv;
	Context context;
	String cancel_orderid,url22=null;
	String order_id2=null;
	Context c;
	
		// JSON Node names
	private static final String TAG_ORDERS = "orders";
	private static final String TAG_ID = "id";
	private static final String TAG_NAME = "name";
	private static final String TAG_ORDER = "order";
	private static final String TAG_RATE = "rate";
	private static final String TAG_QUANTIITY = "quantiity";
	
	// contacts JSONArray
	JSONArray orders = null;
	JSONArray list =null;
	JSONArray orders_details=null;

	// Hashmap for ListView
	ArrayList<HashMap<String, HashMap<String,Float>>> contactList;

	List<String> orderids = new ArrayList<String>();
	ExpandableListView listView ;
	Activity act;
	
	
	//end of code_other
	
  // more efficient than HashMap for mapping integers to objects
  SparseArray<Group> groups = new SparseArray<Group>();

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_cancel_order);
    listView = (ExpandableListView) findViewById(R.id.listView);
    act=CancelOrder.this;
    System.out.println("in oncreate act value "+act);
    new GetContacts().execute();
   
     
  }

  

  public final class myDialogFragment extends DialogFragment{

		public Dialog onCreateDialog(Bundle savedInstanceState) {


			Log.d("inside","test");
			System.out.println("inside on cretae dialog function");
			// Use the Builder class for convenient dialog construction
			AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
			System.out.println("builder value is "+builder);
			builder.setTitle("confirm cancellation").setMessage("Are you sure to cancel order "+cancel_orderid+"?")
			.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int id) {

					//implementation of OK part
					new async().execute();


				}
			})
			.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int id) {
					// User cancelled the dialog


				}
			});
			// Create the AlertDialog object and return it
			return builder.create();
		}

	}


  
  public class async extends AsyncTask<Void, Void, Void>{

		String val;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			// Showing progress dialog
			pDialog = new ProgressDialog(CancelOrder.this);
			pDialog.setMessage("Please wait...");
			pDialog.setCancelable(false);
			pDialog.show();

		}

		@Override
		protected Void doInBackground(Void... arg0) {
			// TODO Auto-generated method stub
			try
			{


				Log.d("DoInBackground","In cancelOrder");
				CancelOrderServiceHandler sh1 = new CancelOrderServiceHandler();

				// Making a request to url2

				try{
					val=sh1.makeServiceCall(url22, CancelOrderServiceHandler.GET);
					JSONObject jsonObj = new JSONObject(val);
					val=jsonObj.getString("value");
					System.out.println("The value in json ouput is "+val);

				}
				catch(Exception e)
				{
					System.out.println("exception in url2 is "+e);
					
				}
			}
			catch(Exception e)
			{
				System.out.println("exception in async task is "+e);
			}




			return null;
		}

		protected void onPostExecute(Void param) {
			//Print Toast or open dialog

			// Dismiss the progress dialog
			if (pDialog.isShowing())
				pDialog.dismiss();

			CharSequence text="null";
			int duration = Toast.LENGTH_LONG;

			if(val.equals("true"))
				text = "succesfull";
			else
				text = "error";

			Toast.makeText(context, text, duration).show();	

			Intent i = new Intent(context, CancelOrder.class);
			startActivity(i); 

		}

  }
  


private class GetContacts extends AsyncTask<Void, Void, Void> {


	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		// Showing progress dialog
		pDialog = new ProgressDialog(CancelOrder.this);
		pDialog.setMessage("Please wait...");
		pDialog.setCancelable(false);
		pDialog.show();

	}

	@Override
	protected Void doInBackground(Void... arg0) {
		
		System.out.println("do in background of Getcontacts");
		// Creating service handler class instance
		CancelOrderServiceHandler sh = new CancelOrderServiceHandler();

		// Making a request to url and getting response
		System.out.println("before calling url");
		sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
       String  memberNumber=sharedPref.getString("memberNumber","");
		String jsonStr=null;
		
		String url = "http://ruralivrs.cse.iitb.ac.in/AFC/IvrsServlet?req_type=getOrderss&number="+memberNumber+"&unprocessed=true";
		try{
		jsonStr = sh.makeServiceCall(url, CancelOrderServiceHandler.GET);
		}
		catch(Exception e){System.out.println("exception in service call is "+e);}
		System.out.println("after calling url");
		Log.d("Response: ", "> " + jsonStr);

		if (jsonStr != null) {
			try {
				
				System.out.println("inside if");
				JSONObject jsonObj = new JSONObject(jsonStr);
				System.out.println("saketh");
				// Getting JSON Array node
				orders = jsonObj.getJSONArray(TAG_ORDERS);

				// looping through All Contacts
				HashMap<String ,HashMap<String ,String>> map1=new HashMap<String,HashMap<String,String>>();
				
				Log.d("length of the order",orders.length()+"");
				for (int i = 0; i < orders.length(); i++) {
					JSONObject c = orders.getJSONObject(i);

					String id = c.getString(TAG_ID);
					orders_details = c.getJSONArray("order");
										
				      Group group = new Group(id);
				      for (int j = 0; j < orders_details.length(); j++) {
				    	
				    	  JSONObject d;
						try {
							d = orders_details.getJSONObject(j);

							String name = d.getString("name");
							String quantiity = d.getString("quantiity");
							Log.d("testing fr cancel" , name );
							 group.children.add(name+"   "+quantiity+" Kg");
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

				      }
				      groups.append(i, group);
				    }
				
				
				
				Log.d("saketh","after 2 fors");






			} catch (JSONException e) {
               System.out.println("The error in do in background is "+e);			}
		} else {
			Log.e("ServiceHandler", "Couldn't get any data from the url");
		}

		return null;
	}

	@Override
	protected void onPostExecute(Void result) {
		super.onPostExecute(result);
		// Dismiss the progress dialog
		if (pDialog.isShowing())
			pDialog.dismiss();
		/**
		 * Updating parsed JSON data into ListView
		 * */
		
		System.out.println("in postexecute act value "+act);
		System.out.println("in post execute groups value "+groups);
	  
		
		final CancelOrderListAdapter adapter = new CancelOrderListAdapter(act, groups);
		    listView.setAdapter(adapter);
		 
		    listView.setOnGroupExpandListener(new OnGroupExpandListener() {
		        int previousItem = -1;

		        @Override
		        public void onGroupExpand(int groupPosition) {
		            if(groupPosition != previousItem )
		                listView.collapseGroup(previousItem );
		            previousItem = groupPosition;
		        }
		    });
		 		//implement cancel order option in on click listener 

		System.out.println("implementing cancel");
		    
		    listView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
				
				public boolean onChildClick(ExpandableListView parent, View v, int gp,int cp, long arg4) {
					
					// TODO Auto-generated method stub
					System.out.println("inside onchildclick");
					Log.d("jjdk","saketh");
					System.out.println("inside lv listener saketh");
					String order_id=null;
					try
					{
					 order_id = ((Group) adapter.getGroup(gp)).getString();
					
					}
					catch(Exception e){System.out.println("error in getting group name is "+e);}
					cancel_orderid=order_id;
					
					System.out.println("inside lv listener,printing order id" +order_id);
					sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
				       String  memberNumber=sharedPref.getString("memberNumber","");
					
					final String url2 = "http://ruralivrs.cse.iitb.ac.in/AFC/IvrsServlet?req_type=cancelOrder&orderId="+order_id+"&mobileNumber="+memberNumber;
					
					System.out.println("the delete query is " +url2);
					
					url22=url2;
					order_id2=order_id;

					//implementing alert dialog

					c=CancelOrder.this;
					context = getApplicationContext();
					myDialogFragment ob2=new myDialogFragment();
					//  Dialog ob1=new Dialog(c);


					android.app.FragmentManager fm=getFragmentManager();
					ob2.show(fm,"1");
		
					return true;
				}
			});
		}

	}

}


