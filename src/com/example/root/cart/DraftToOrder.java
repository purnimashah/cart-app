package com.example.root.cart;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;
import org.xml.sax.SAXException;

import com.example.root.cart.ConfirmOrderActivity.WriteToServer;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Parcelable;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class DraftToOrder extends Activity implements OnClickListener{
	 
	Button b1 ;
	EditText t1;
	 String memberNumber;
	  ArrayList<ItemRow> items;
	  int j=1;
//	  SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
	  
     Intent n = getIntent();
	  protected void onCreate(Bundle savedInstanceState) {
      	Log.d("test","inside lengtofselected item0");
		  
	        super.onCreate(savedInstanceState);
	        setContentView(R.layout.activity_draft_order_final);
			        
	        Bundle b = getIntent().getExtras();
	        String[] resultArr = b.getStringArray("selectedItems");
	        int lengthOfSelectedItem = resultArr.length;
	        items = new ArrayList<ItemRow>();
	        
	        if(lengthOfSelectedItem<0){
	        	Log.d("test","inside lengtofselected item");
	        	Toast.makeText(DraftToOrder.this,"No order is here", Toast.LENGTH_LONG).show();
				 Intent intent = new Intent(getApplicationContext(),
			                DashboardActivity.class);
				 startActivity(intent);
	        }
	        else
	        for(int i =0 ; i<lengthOfSelectedItem ; i ++){
	        	 
	        	 int aa = resultArr[i].indexOf("\n");
	     		String name = resultArr[i].substring(5,aa);
	     		int aa1 =resultArr[i].indexOf("Quantity:");
	     		int aa2 = resultArr[i].indexOf("Price");
	     		int aa3 = resultArr[i].indexOf("Total");
	     		String quantity= resultArr[i].substring(aa1+9 , aa2-2);
	     		String price = resultArr[i].substring(aa2+6, aa3-2);
	     		String total = resultArr[i].substring(aa3+6);
	     		
	     		Log.d("test string",name);
	     		Log.d("test string",total);
	     		ItemRow item = new ItemRow(name , Double.parseDouble(quantity), Double.parseDouble(price),Double.parseDouble(total));
	     		Log.d("purnima", "item add");
                items.add(item);
	        }
	        Log.d("Products","Items in items " + items.toString());
	       
	        ListView lv = (ListView) findViewById(R.id.outputList);
	        b1 = (Button) findViewById(R.id.button_modify_order);
	        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
	                android.R.layout.simple_list_item_1, resultArr);
	        lv.setAdapter(adapter); 
	        b1.setOnClickListener(this);
	  }

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if(v== b1){
			
			SQLiteDatabase db = this.getReadableDatabase();
			OrdersDBHelper dd = new OrdersDBHelper(getBaseContext());
			dd.getWritableDatabase(); 
			db= openOrCreateDatabase("Order_drafts.db", SQLiteDatabase.CREATE_IF_NECESSARY, null); 
			
			SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
            memberNumber=sharedPref.getString("memberNumber","");
            String groupId = sharedPref.getString("groupId", "");
		    Log.d("numbetrreete",groupId);
		  //  int groupId = Integer.parseInt(getgroupId);
            
         //   String groupId="54";

            JSONObject object= new JSONObject();
            try {
                JSONObject order = new JSONObject();
                order.put("memberNumber", memberNumber);
                order.put("groupId", groupId);
                
                JSONObject products = new JSONObject();
                
                int i=1;
                for(ItemRow item : items){
                    products.put(item.getName(), item.getQuantity().toString());
                    i++;
                  
                }
                order.put("products",products);
                object.put("order",order);

            } catch (Exception e) {
                e.printStackTrace();
            }

            Log.d("Order_JSON",object.toString());
            
            //ConfirmOrderActivity ca = new ConfirmOrderActivity();
            //ca.new WriteToServer().execute(b);
            new WriteToServer().execute(object.toString());
          

            for(ItemRow item : items){
            	Log.d("Delete Query = ", "Delete from draftss where COLUMN_NAME_PRODUCT_NAME="+item.getName() +" AND"+" quantity="+item.getQuantity());
            	
            	 db.execSQL("Delete from draftss where product_name ='"+item.getName()+"'AND quantity='"+item.getQuantity()+"'");
                j++;
              
            }
          
            Intent i = new Intent(this,OrderSubmitActivity.class);
            startActivity(i);
			
		}
	}
	private SQLiteDatabase getReadableDatabase() {
		// TODO Auto-generated method stub
		return null;
	}
	
	 public void saveOrder(int orderNumber, String memberNumber) throws FileNotFoundException, SAXException {

	        OrdersDBHelper ordersDb = new OrdersDBHelper(getApplicationContext());
	        OrderedProductsDBHelper productsDb = new OrderedProductsDBHelper(getApplicationContext());

	        long ref_num = ordersDb.getReferenceNumber();
	       // int groupId = 54; //TODO: get group ID
	        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
	        String getgroupId = sharedPref.getString("groupId", "");
		    Log.d("numbetrreete",getgroupId);
		    int groupId = Integer.parseInt(getgroupId);
	        long res = ordersDb.insertOrder(orderNumber, groupId, memberNumber, ref_num);
	        if(res==-1){
	            Log.d("DB_ERROR","Could not insert order");
	        }

	        Log.d("Cart","Executing saveOrder");
	        try {

	            for (ItemRow item : items) {
	                if(productsDb.saveProduct(orderNumber,item.getName(),item.getQuantity(),item.getUnitPrice(),item.getTotal())){
	                    ;
	                }
	                else{
	                    Log.d("DB_ERROR","Could not insert product");
	                }
	            }

	        } catch (Exception e) {
	            e.printStackTrace();
	        }

	        Log.d("Orders",ordersDb.getOrdersByNumber(memberNumber).toString());
	    }

	    public class WriteToServer extends AsyncTask<String, String, Integer> {

	        protected Integer doInBackground(String... params) {
	            Integer orderId=-1;
	            try {

	                String serverUrl = "http://ruralivrs.cse.iitb.ac.in/AFC/IvrsServlet?req_type=addOrder&order=" + URLEncoder.encode(params[0], "UTF-8");
	                DefaultHttpClient httpClient = new DefaultHttpClient();
	                HttpPost httpPost = new HttpPost(serverUrl);
	                HttpResponse httpResponse = httpClient.execute(httpPost);
	                HttpEntity httpEntity = httpResponse.getEntity();
	                InputStream is = httpEntity.getContent();
	                BufferedReader reader = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);
	                StringBuilder sb = new StringBuilder();
	                String line = null;
	                while ((line = reader.readLine()) != null) {
	                    sb.append(line);
	                }
	                is.close();
	                Log.d("response",sb.toString());
	                orderId = Integer.parseInt(sb.toString());

	            } catch (Exception e) {
	                e.printStackTrace();
	            }
	            Log.d("cart",orderId.toString());

	            return orderId;
	        }

	        protected void onPostExecute(Integer result) {
	            if (result >= 0) {
	                try {
	                    saveOrder(result,memberNumber);
	                } catch (FileNotFoundException e) {
	                    e.printStackTrace();
	                } catch (SAXException e) {
	                    e.printStackTrace();
	                }
	            }
	            else
	                //TODO: Failed!
	                ;
	        }
	    }

	}

