package com.example.root.cart;

/**
 * Created by root on 17/12/14.
 */
public class ItemRow {

    private String name;
    private Double quantity;
    private Double unitPrice;
    private Double total;

    public ItemRow(String name, Double quantity, Double unitPrice, Double total) {
        this.name = name;
        this.quantity = quantity;
        this.unitPrice = unitPrice;
        this.total = total;
    }

  

	public String getName() {
        return name;
    }

    public Double getQuantity() {
        return quantity;
    }

    public Double getUnitPrice() {
        return unitPrice;
    }

    public Double getTotal() {
        return total;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setQuantity(Double quantity) {
        this.quantity = quantity;
    }

    public void setUnitPrice(Double unitPrice) {
        this.unitPrice = unitPrice;
    }

    public void setTotal(Double total) {
        this.total = total;
    }
}
