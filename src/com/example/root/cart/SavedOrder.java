package com.example.root.cart;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.database.Cursor;

/**
 * Created by root on 23/1/15.
 */
public class SavedOrder {

    String memberNumber;
    int order_id;
    int group_id;
    int order_ref_num;
    String timeStamp;
    String status;
    //ArrayList<OrderRow> items;
    HashMap<String,String[]> itemList;

    public SavedOrder(Cursor c)
    {
		itemList=new HashMap<String, String[]>();
        memberNumber = c.getString(c.getColumnIndex(OrdersDBHelper.OrdersEntry.COLUMN_NAME_MEMBER_NUMBER));
        order_id = c.getInt(c.getColumnIndex(OrdersDBHelper.OrdersEntry.COLUMN_NAME_ORDER_ID));
        order_ref_num = c.getInt(c.getColumnIndex(OrdersDBHelper.OrdersEntry.COLUMN_NAME_ORDER_REFERENCE_NO));
        group_id = c.getInt(c.getColumnIndex(OrdersDBHelper.OrdersEntry.COLUMN_NAME_GROUP_ID));
        status = c.getString(c.getColumnIndex(OrdersDBHelper.OrdersEntry.COLUMN_NAME_STATUS));
        timeStamp = c.getString(c.getColumnIndex(OrdersDBHelper.OrdersEntry.COLUMN_NAME_TIMESTAMP));
    }
    
    public SavedOrder(JSONObject order,String memberNumber){
    	
    	try {
			this.memberNumber=memberNumber;
			this.order_id=order.getInt("id");
			this.group_id=order.getInt("groupId");
			this.timeStamp=order.getString("orderTime");
			JSONArray orderItems=order.getJSONArray("order");
            itemList=new HashMap<String, String[]>();
            for(int i=0;i<orderItems.length();i++){
				JSONObject item=(JSONObject)orderItems.get(i);
                String name=item.getString("name");
                String[] temp=new String[2];
                temp[0]=item.getString("rate");
                temp[1]=item.getString("quantiity");
                itemList.put(name,temp);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
    	
    }

    public String getMemberNumber() {
        return memberNumber;
    }

    public void setMemberNumber(String memberNumber) {
        this.memberNumber = memberNumber;
    }

    public int getOrder_id() {
        return order_id;
    }

    public void setOrder_id(int order_id) {
        this.order_id = order_id;
    }

    public int getOrder_ref_num() {
        return order_ref_num;
    }

    public void setOrder_ref_num(int order_ref_num) {
        this.order_ref_num = order_ref_num;
    }

    public int getGroup_id() {
        return group_id;
    }

    public void setGroup_id(int group_id) {
        this.group_id = group_id;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    
    public String getItems() {
    	String res="";
    	for(String itemName:itemList.keySet()){
    		res+= itemName + "-" + itemList.get(itemName)[1] + "\n";
    	}
    	return res;
    }
    
    public HashMap<String,String[]> getItemList() {
    	return itemList;
    }
}
