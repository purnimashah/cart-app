package com.example.root.cart;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Parcelable;
import android.preference.PreferenceManager;
import android.util.Log;
import android.util.Xml;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.io.StringWriter;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.JSONException;
import org.json.JSONObject;
import org.xml.sax.SAXException;
import org.xmlpull.v1.XmlSerializer;


public class ConfirmOrderActivity extends Activity implements View.OnClickListener{

	Button b1,b2;
	int grandTotal;
	ArrayList<ItemRow> items;
	String memberNumber;
	Context context;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_confirm_order);
		context = getApplication();
		ListView list = (ListView) findViewById(R.id.confirm_order_item_list);
		Log.d("Parag",String.valueOf(getItems().isEmpty()));
		
		List<ItemRow> item_list = getItems();
		ItemRowAdapter adapter = new ItemRowAdapter(this,R.layout.view_order_row, item_list);
		list.setAdapter(adapter);
		TextView totalBill = (TextView) findViewById(R.id.confirm_order_total_bill);
		totalBill.setText(grandTotal+"");

		b2=(Button)findViewById(R.id.button_checkout);
		b2.setOnClickListener(this);

		b1=(Button)findViewById(R.id.button_modify_order);
		b1.setOnClickListener(this);
	}


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.menu_dashboard, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();

		//noinspection SimplifiableIfStatement
		if (id == R.id.action_home) {
			Intent i = new Intent(this,DashboardActivity.class);
			startActivity(i);
			return true;
		}

		return super.onOptionsItemSelected(item);
	}

	public List<ItemRow> getItems() {

		items = new ArrayList<ItemRow>();

		Map<String,String> itemsMap = (Map) getIntent().getSerializableExtra("itemsMap");
		Map<String, String> sortList = new TreeMap<String, String>(itemsMap);
		Log.d("Products","Item Map in getItems is "+ itemsMap.toString());
		for(Map.Entry<String, String> entry : sortList.entrySet()){
			double quantity = getIntent().getDoubleExtra(entry.getKey(),0);
			if(quantity > 0) {
				double total = quantity * Double.parseDouble(entry.getValue());
				grandTotal += total;
				ItemRow item = new ItemRow(entry.getKey(), quantity, Double.parseDouble(entry.getValue()), total);
				items.add(item);
			}
		}
		Log.d("Products","Items in items " + items.toString());


		return items;
	}

	@Override
	public void onClick(View view) {

		if(view==b1){
			Intent i = new Intent(this,NewOrderActivity.class);
			i.putExtras(getIntent().getExtras());
			startActivity(i);
		}
		else if(view==b2){

			SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

			memberNumber=sharedPref.getString("memberNumber","");
		    String groupId = sharedPref.getString("groupId", "");
		    Log.d("numbetrreete",groupId);
		
		    if(grandTotal<=0){
		    	
		    	Log.d("grandtotal","inside <0");
		    	
		    	Toast.makeText(getApplicationContext(), "Please select atleast one product",
		    			   Toast.LENGTH_LONG).show();
		    }
		    else {
			JSONObject object= new JSONObject();
			try {
				JSONObject order = new JSONObject();
				order.put("memberNumber", memberNumber);
				order.put("groupId", groupId);
				JSONObject products = new JSONObject();
				int i=1;
				for(ItemRow item : items){
					products.put(item.getName(), item.getQuantity().toString());
					i++;
				}
				order.put("products",products);
				object.put("order",order);

			} catch (Exception e) {
				e.printStackTrace();
			}

			Log.d("Order_JSON",object.toString());
			
			new WriteToServer().execute(object.toString());
			
			Intent i = new Intent(this,OrderSubmitActivity.class);
			startActivity(i);
		    }
		    }
		    
	}



	public class WriteToServer extends AsyncTask<String, String, Integer> {

		protected Integer doInBackground(String... params) {
			Integer orderId=-1;
			try {
				
				String serverUrl = "http://ruralivrs.cse.iitb.ac.in/AFC/IvrsServlet?req_type=addOrder&order=" + URLEncoder.encode(params[0], "UTF-8");
				DefaultHttpClient httpClient = new DefaultHttpClient();
				HttpPost httpPost = new HttpPost(serverUrl);
				HttpResponse httpResponse = httpClient.execute(httpPost);
				HttpEntity httpEntity = httpResponse.getEntity();
				InputStream is = httpEntity.getContent();
				BufferedReader reader = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);
				StringBuilder sb = new StringBuilder();
				String line = null;
				while ((line = reader.readLine()) != null) {
					sb.append(line);
				}
				is.close();
				Log.d("response",sb.toString());
				orderId = Integer.parseInt(sb.toString());

				//response.getParams().getParameter("");
			} catch (Exception e) {
				e.printStackTrace();
			}
			Log.d("cart",orderId.toString());

			return orderId;
		}

		protected void onPostExecute(Integer result) {
			if (result >= 0) {
				try {
					saveOrder(result,memberNumber);
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				} catch (SAXException e) {
					e.printStackTrace();
				}
			}
			else
				//TODO: Failed!
				;
		}

		public void saveOrder(int orderNumber, String memberNumber) throws FileNotFoundException, SAXException {
	
			try{
				Log.d("save order","saving the order in database");
				OrdersDBHelper ordersDb = new OrdersDBHelper(context);
				OrderedProductsDBHelper productsDb = new OrderedProductsDBHelper(context);
				long ref_num = ordersDb.getReferenceNumber();
				SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
			    String imgSett = sharedPref.getString("groupId", "");
				//int groupId = 54; //TODO: get group ID
				int groupId = Integer.getInteger(imgSett);
				long res = ordersDb.insertOrder(orderNumber, groupId, memberNumber, ref_num);
				if(res==-1){
					Log.d("DB_ERROR","Could not insert order");
				}

				Log.d("Cart","Executing saveOrder");
				try {

					for (ItemRow item : items) {
						if(productsDb.saveProduct(orderNumber,item.getName(),item.getQuantity(),item.getUnitPrice(),item.getTotal())){
							;
						}
						else{
							Log.d("DB_ERROR","Could not insert product");
						}
					}

				} catch (Exception e) {
					e.printStackTrace();
				}

				Log.d("Orders",ordersDb.getOrdersByNumber(memberNumber).toString());
			}
			catch(Exception e){
				e.printStackTrace();
			}

		}
	}
}
